/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/v1/tenants              ->  index
 * POST    /api/v1/tenants              ->  create
 * GET     /api/v1/tenants/:id          ->  show
 * PUT     /api/v1/tenants/:id          ->  update
 * DELETE  /api/v1/tenants/:id          ->  destroy
 */
 const errors = require('throw.js');
 import * as gammaConfig from './../../../core/config';
 import * as db from './../../../component/db';
 import * as cf from './../../../utils/common-functions';
 import * as emailSender from '../../../component/email';

// Gets a list of Tenants
export function index(req, res, next) {
console.log(1)
    res.status(200).json([]);
}

// Get a single tenant
export async function show(req, res, next) {
    let sql_query = "select * from get_tenant_summary_details($1)";
    db.gammaDbPool.query(sql_query, [req.params.tenantId], next)
        .then(result => {
            res.status(200).json(result[0]);
        });
}

export async function create(req, res, next) {
    let firstName, lastName, email, password, sendEmail, isVerify, isTrial;
    let tenantUid;
    try {
        if (req.body.tenantUid == undefined)
            tenantUid = new Date().getTime();
        else
            tenantUid = cf.parseString(req.body.tenantUid);
            
        firstName = cf.parseString(req.body.firstName);
        lastName = cf.parseString(req.body.lastName);
        email = cf.parseString(req.body.email);
        if (req.body.password == undefined)
            password = '';
        else
            password = cf.parseString(req.body.password);

        subdomain = req.body.subdomain;
        req.subdomain = req.body.subdomain;
        isTrial = (req.body.isTrial == 'false' || req.body.isTrial === false) ? false : true;
        if (req.body.subdomain === 'local') {
            isTrial = false;
        }
        req.tenant_uid = tenantUid;

        sendEmail = (req.body.sendEmail) ? req.body.sendEmail : true;
        isVerify = (req.body.isVerify == 'false' || req.body.isVerify === false) ? false : true;
    } catch (err) {
        return next(new errors.BadRequest(null, 1000));
    }
    return addAccount(req, next, firstName, lastName, email, password, isTrial, sendEmail, isVerify, tenantUid)
    .then(function(){
        res.status(200).json({
            status: 'success',
            message: 'Account added successfully.',
            details: 'Account added successfully.',
        });
    });
}
export function addAccount(req, next, firstName, lastName, email, password, isTrial, sendEmail, isVerify, tenantUid) {
    if (req.body.subdomain == undefined) {
        if (req.subdomains.length > 1) {
            if (_.contains(req.subdomains, gammaConfig.gamma_os_postfix)) {
                req.body.subdomain = _.last(req.subdomains);
            } else {
                req.body.subdomain = 'local';
            }
        } else
            req.body.subdomain = _.last(req.subdomains);
        if (!req.body.subdomain)
            req.body.subdomain = "local";
    }
    try {
        let salt = cf.makeSalt();
        let hashedPassword = (password == "") ? "" : cf.encryptPassword(password, salt);
        let registrationTransaction = `select * from register_tenant('${tenantUid}','${firstName}','${lastName}','${email}','${hashedPassword}','${subdomain}',${isTrial},'${salt}',${isVerify})`;
        return db.gammaDbPool.query(registrationTransaction, [], next)
            .then(account => {
                return new Promise((resolve, reject) => {
                    if (account[0].register_tenant == 'SUCCESS') {
                        let updateTenant = `update tenant set is_migrated=true  where tenant_uid = $1`;
                        return db.gammaDbPool.query(updateTenant, [tenantUid], next);
                        // let requestbody = {
                        //     tenant_uid: tenantUid,
                        //     log: "1 user added with email:" + email,
                        //     metrics: [{
                        //         metric: "users",
                        //         value: 1
                        //     }]
                        // };
                        // updateUsage(requestbody);
                        if (sendEmail && sendEmail != 'false')
                            emailSender.sendMail('welcome-email', {
                                'subject': 'Welcome aboard Gamma!',
                                'base_url': gammaConfig.gamma_ui_public_host,
                                'web_url': cf.getWebSiteURL(),
                                'email_type': "welcome-email",
                                'email': email,
                                'username': email,
                                'first_name': firstName,
                                'link': '',
                                /*  'password': password, */
                                'gamma_url': gammaConfig.gamma_ui_public_host + gammaConfig.root,
                                'image_url': gammaConfig.gamma_ui_public_host,
                                'is_primary': true,
                                'licence_url': gammaConfig.gamma_ui_public_host + '/licence_url'
                            });
                        db.addDBToAccount(req, res, next, (error) => {
                            //if error , then there was something wrong while creating schema in corona db. So deleting newly created tenant info from gamma db

                            if (error) {
                                log.debug(error);
                                return reverseTransaction(req, next, tenantUid)
                                .then(function(){
                                    let errorMessage = 'There was error in creating database. Something went wrong while creating database. Please try again later.'
                                    reject(new errors.InternalServerError(errorMessage, 1024));
                                });
                            } else{
                                resolve('');
                            }
                                
                        });

                    } else {
                        reject(new errors.CustomError("DuplicateUserError", "Duplicate user, tenant already exists", 400, 1101));
                    }
                });
            });
    } catch (err) {
        return next(new errors.InternalServerError(err.message, 1024));
    }
}
export function reverseTransaction(req, next, tenantUid){
    sqlQuery = `DO $$
                BEGIN
                delete from tenant where tenant_uid = '${tenantUid}';
                delete from tenant_db_info where tenant_uid = '${tenantUid}';
                END;$$`;
    return req.gamma.query(sqlQuery, [req.params.kpiId], next)
        .then(function(data) {
            console.log(data);
            //return new Promise((resolve, reject) => {
                return true;
           // });
        });
}